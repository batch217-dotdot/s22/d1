// Section - Array methods

// Mutator methods

// push
let fruits = ["Apple", "Orange", "Kiwi", "Dragon fruits"];
console.log("Current array:");
console.log(fruits);

let fruitsLength = fruits.push("Mango");
console.log(fruitsLength);
console.log("Mutated array from push method:");
console.log(fruits);

fruits.push("Avocado", "Guava");
console.log("Mutated array from push method:");
console.log(fruits);

// pop()
let removedFruit = fruits.pop();
console.log(removedFruit);
console.log("Mutated array from pop method:");
console.log(fruits);
fruits.pop();
console.log("Mutated array from pop method:");
console.log(fruits);

// unshift()
fruits.unshift("Lime", "Banana");
console.log("Mutated array from unshift method:")
console.log(fruits);

// shift()
let anotherFruit = fruits.shift();
console.log(anotherFruit);
console.log("Mutated array from shift method:")
console.log(fruits);

// splice()
// arrayName.splice(startingIndex, deleteCount, elemtentsTobeAdded)
fruits.splice(1, 2, "Lime", "Cherry");
console.log("Mutated array from splice method:")
console.log(fruits);

// sort()
fruits.sort();
console.log("Mutated array from sort method:")
console.log(fruits);

// reverse()
fruits.reverse();
console.log("Mutated array from reverse method:")
console.log(fruits);

// Non-mutator methods

let countries = ["US", "PH", "CAN", "SG", "TH", "PH", "FR", "DE"];

// indexOf()
// Syntax --> arrayName.indexOf(SearchValue);
// Syntax --> arrayName.indexOf(SearchValue, fromIndex);

let firstIndex = countries.indexOf("PH");
console.log("Result of indexOf method: " + firstIndex); 

let invalidCountry = countries.indexOf("BR");
console.log("Result of indexOf method: " + invalidCountry);

// lastIndexOf()
// syntax --> arrayName.lastIndexOf(searchValue)

let lastIndex = countries.lastIndexOf("PH");
console.log("Result of lastIndexOf method: " + lastIndex);

let lastIndexStart = countries.lastIndexOf("PH", 6);
console.log("Result of lastIndexOf method: " + lastIndexStart);

// slice()
let slicedArrayA = countries.slice(2);
console.log("Result from slice method:");
console.log(slicedArrayA);

let slicedArrayB = countries.slice(2, 4);
console.log("Result from slice method:");
console.log(slicedArrayB);

let slicedArrayC = countries.slice(-3);
console.log("Result from slice method:");
console.log(slicedArrayC);

// toString()
let stringArray = countries.toString();
console.log("Result from toString method:");
console.log(stringArray);

// concat()
let tasksArrayA = ["drink html", "eat javascript"];
let tasksArrayB = ["inhale css", "breath sass"];
let tasksArrayC = ["get git", "be node"];

let tasks = tasksArrayA.concat(tasksArrayB);
console.log("Result from concat method:");
console.log(tasks);

// combining multiple arrays
console.log("Result from concat method:");
let allTasks = tasksArrayA.concat(tasksArrayB, tasksArrayC);
console.log(allTasks);

// combining arrays with elements
let combinedTasks = tasksArrayA.concat("smell express", "throw react");
console.log("Result from concat method:");
console.log(combinedTasks);


// join()
let users = ["John", "Jane", "Joe", "Robert"];
console.log(users.join());
console.log(users.join(''));
console.log(users.join(' - '));
console.log(users.join(' ! '));

// Iteration methods

// forEach()
// arrayName.forEach(function(indivElement){statement})

allTasks.forEach(function(task){
	console.log(task);
});

// using forEach with conditional statements
let filteredTasks = [];

// looping through all array items

allTasks.forEach(function(task) {
	if(task.length > 10) {
		filteredTasks.push(task);
	}
});
console.log("Result of filtered tasks:")
console.log(filteredTasks);

// map()
// syntax let/consst resultArray = arrayName.map(function(indivElement))

let numbers = [1, 2, 3, 4, 5];

let numberMap = numbers.map(function(number) {
	return number * number;
});
console.log("Original Array:")
console.log(numbers);
console.log("Result of map method:")
console.log(numberMap);

// map() vs forEach()

let numberForEach = numbers.forEach(function(number) {
	return number * number;
})
console.log(numberForEach); // undefined result

// every()
// syntax --> let/const resultArray = arrayName.every(function(indivElement){return expression/condition;})

let allValid = numbers.every(function(number) {
	return (number > 0);
});
console.log("Result of every method:")
console.log(allValid);

// some()

let someValid = numbers.some(function(number){
	return (number < 2);
});
console.log("Result from some method:")
console.log(someValid);

if(someValid) {
	console.log("Some numbers in the array are greater than 2");
}

// filter()
// syntax --> let/const resultArray = arrayName.filter(function(indivElement){return expression/condition})

let filterValid = numbers.filter(function(number) {
	return (number < 3);
});
console.log("Result from filter method:")
console.log(filterValid);

// No element found
let nothingFound = numbers.filter(function(number) {
	return (number = 0);
});
console.log("Result from filter method:")
console.log(nothingFound); // it will return empty brackets

// filtering using forEach

let filteredNumbers = [];

numbers.forEach(function(number) {
	if(number < 3) {
		filteredNumbers.push(number);
	}
})
console.log("Result from filter method:")
console.log(filteredNumbers);

// includes()
// syntax --> arrayName.includes(<argument>)

let products = ["Mouse", "Keyboard", "Laptop", "Monitor"];

let productFound1 = products.includes("Mouse");
console.log(productFound1);

let productFound2 = products.includes("Headset");
console.log(productFound2);

// reduce()
/*
syntax let/const resultArray = arrayName.reduce(function(accumulator, currentValue){return expression/condition})
*/

let iteration = 0;
let reduceArray = numbers.reduce(function(x, y) {
	console.warn("current iteration: " + ++iteration);
	console.log("accumulator: " + x);
	console.log("currentValue: " + y);

	return x + y;
});
console.log("Result of reduce method: " + reduceArray);